# -*- coding: utf-8 -*-
import xml.sax
from insertDrugStores import engine, metadata , insert ,select
from sqlalchemy import Table , table , column
import datetime

rest_table = Table('rest', metadata , autoload = True , autoload_with = engine)
goods_table = table('goods', column('goods_id') )
drug_store_table = table('drug_stores', column('id'))


class ParseXMLSax(xml.sax.ContentHandler):

    def __init__(self):
        self.CurrentData = ""
    

    def startElement(self, tag, attrs):
        self.CurrentData = tag

        if tag == 'rest':
            # print('attr quantity: ' + str(tag['quantity']))

            goods_q = select([goods_table]).where(goods_table.c.goods_id == int(attrs['idgood']))
            drug_store_q = select([drug_store_table]).where(drug_store_table.c.id == int(attrs['idbranch']))
            res_good = engine.execute(goods_q)
            res_drug_store = engine.execute(drug_store_q)
            if res_good.fetchone and res_drug_store.fetchone:
                # print('--------------')
                # print(res_good.fetchone())
                # print(res_drug_store.fetchone())
            # inserting values from xml to rest table
                operation = insert(rest_table).values({
                    'good_id': int(attrs['idgood']),
                    'branch_id': int(attrs['idbranch']),
                    'quantity': str(attrs['quantity']),
                    'price': str(attrs['price']),
                    'created_at' : datetime.datetime.utcnow(),
                    'updated_at' : datetime.datetime.utcnow()
                })
                engine.execute(operation)

                print("added to database ||| idbranch = {} , idgood = {} , quantity = {} , price =  {}".format(
                    attrs['idbranch'], attrs['idgood'],
                    attrs['quantity'], attrs['price']
                ))




if __name__ == "__main__":
    parser = xml.sax.make_parser()
    parser.setFeature(xml.sax.handler.feature_namespaces,0)
    Handler = ParseXMLSax()
    parser.setContentHandler(Handler)
    parser.parse('2018-11-08/rest.xml')