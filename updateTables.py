from med_files import Medicine

# create instance of class Medicine
medicine = Medicine()

current_day = medicine.current_day
#download and unzip
medicine.download_zip_current_day()
medicine.unzip()
medicine.zip_delete()

xml_files = medicine.get_file_names_in_folder(current_day)

print(current_day)