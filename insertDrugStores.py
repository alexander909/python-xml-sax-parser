from sqlalchemy import create_engine, select , MetaData , Table, insert
import datetime

# create database connection
engine = create_engine('mysql+mysqlconnector://mp_fresh:pHBeAFnVnvPQ2PYw@localhost/mp_fresh')
metadata = MetaData(bind=None)
table = Table('drug_stores', metadata, autoload = True, autoload_with = engine)

# operation = insert(table).values({'created_at': datetime.datetime.utcnow(),
#                                   'updated_at': datetime.datetime.utcnow(),
#                                   'name': 'network 4'})
# engine.execute(operation)

# import xml.etree.ElementTree as ET
#
# xml_file = ET.parse('2018-11-08/pharmacies.xml')
# root_elm = xml_file.getroot()
#
# for child in root_elm:
#     operation = insert(table).values({
#         'id': int(child.attrib['idbranch']),
#         'num': int(child.attrib['num']),
#         'region': child.attrib['region'],
#         'city': child.attrib['city'],
#         'address': child.attrib['address'],
#         'phone': child.attrib['phone'],
#         'work_time': child.attrib['work'],
#         'name': child.text,
#         'created_at': datetime.datetime.utcnow(),
#         'updated_at': datetime.datetime.utcnow()
#     })
#
#     engine.execute(operation)
#     print('idbranch {} has added to database'.format(child.attrib['idbranch']))