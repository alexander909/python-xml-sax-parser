import time
import requests
import zipfile
import os
import datetime
import shutil
import glob
import xml.etree.ElementTree as ET


class Medicine:
    login = "medpravda"
    password = "4z9F3w2H7u"
    url = "https://apteka.net.ua/medpravda.zip"
    # previous day get
    last_day = str(datetime.date.today() - datetime.timedelta(1))
    current_day = time.strftime("%Y-%m-%d")
    zip_file_current_day = current_day + ".zip"

    def download_zip_current_day(self):
        # check if current zip file with current_day name is exist
        if not (os.path.exists(self.current_day + '.zip')):
            # get content from url
            url_content = requests.get(self.url, auth=(self.login, self.password))
            if url_content.status_code == 200:
                # create zip file from response from url request.get()
                with open(self.current_day + ".zip", "wb") as zip_medicine:
                    zip_medicine.write(url_content.content)

    # unzip current zip file
    def unzip(self):
        if os.path.exists(self.zip_file_current_day):
            zp = zipfile.ZipFile(self.zip_file_current_day, 'r')
            zp.extractall(self.current_day)
            zp.close()
            return True
        else:
            return False

    #delete zip file when folder create
    def zip_delete(self):
        folder = os.path.exists(self.current_day)
        if folder:
           os.remove(self.current_day +'.zip')

    def delete_last_day_zipfile_folder(self):
        if self.last_day != self.current_day:
            os.remove(self.last_day + ".zip")
            if os.path.exists(self.last_day):
                self.folder_delete(path=self.last_day)

    def folder_delete(self, path):
        shutil.rmtree("/" + path)

    def get_file_names_in_folder(self, folder_name):
        # os.listdir(folder_name)
        # return array only with files with .xml
        files = [x for x in glob.glob(folder_name + '/*.xml') if x.split('.')[1] == 'xml']
        return files

    def readXMLfile(self, filepath):
        xml = ET.parse(filepath)
        return xml

    # get file size
    def fileSize(self, filepath):
        size = int((os.path.getsize(filepath) / 1024) / 1024)
        return "size File : {} MB".format(size)


# test class Medicine()
medicine = Medicine()


# zip_status = medicine.unzip()
# print(medicine.zip_file_current_day)

# content in folder 
# folder_content = medicine.get_file_names_in_folder('2018-11-06')
# print(folder_content)
# xml_file = medicine.readXMLfile(folder_content[3]).getroot()
# print(medicine.fileSize(folder_content[3]))


if __name__ == '__main__':
    current_day = medicine.current_day
    previus_day = medicine.last_day

    current_files = medicine.get_file_names_in_folder(current_day)
    last_files = medicine.get_file_names_in_folder(previus_day)

    # download current day file
    medicine.download_zip_current_day()
    medicine.unzip()

    print(current_day, previus_day)
